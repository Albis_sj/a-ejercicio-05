import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-condicional',
  templateUrl: './condicional.component.html',
  styleUrls: ['./condicional.component.css']
})
export class CondicionalComponent implements OnInit {

  info1: any = {
    descripcion: 'Es un compositor argentino que comenzó con batallas de rap en plazas de argentina',
    nombre: 'Paulo Londra',
  };

  mostrar= true;
  

  constructor() { }

  ngOnInit(): void {
  }

}
